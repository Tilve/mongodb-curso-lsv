
from bson.objectid import ObjectId
  # noqa
import sys

sys.path.append("..")
from car.databases.client import MongoLsv
mongo_lsv = MongoLsv()

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="mongo_lsv",
        collection="actor_famoso",
        record={
            "first_name": "Robert",
            "last_name": "De niro",
            "Lengua": "ingles",
            "mejor_pelicula": "Taxi Dirver",
            
        },
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="mongo_lsv",
        collection="actor_famoso",
        record={
            "first_name": "William",
            "last_name": "Bradley Pitt",
            "Lengua": "ingles",
            "mejor_pelicula": "El Club de la Pelea",
            
        },
    )
)


print(
    mongo_lsv.get_records_from_collection(
        db_name="mongo_lsv", collection="actor_famoso"
    )
)


print(
    mongo_lsv.create_new_record_in_collection(
        db_name="mongo_lsv",
        collection="pais",
        record={
            "pais": "Estados Unidos",
            "ciudad":"Nueva York",
            "lengua": "Ninguna",
            
        },
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="mongo_lsv",
        collection="pais",
        record={
            "pais": "Estados Unidos",
            "ciudad":"Oklahoma",
            "lengua": "Ninguna",
            
        },
    )
)


print(
    mongo_lsv.get_records_from_collection(
        db_name="mongo_lsv", collection="pais"
    )
)
'''
Codigo para crear un nuevo registro en la colección universities
'''

