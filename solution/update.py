from bson.objectid import ObjectId
import sys

sys.path.append("..")
from car.databases.client import MongoLsv

mongo_lsv = MongoLsv()
print(
    mongo_lsv.update_record_in_collection(
        db_name="persons",
        collection="actor_famoso",
        record_query={"_id": ObjectId('62f40218626954d099f69ede')},
        record_new_value={
            'first_name': 'Travis',
            'last_name': 'Bickle',
            'age': 31,
        },
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="actor_famoso"
    )
)

print(
    mongo_lsv.update_record_in_collection(
        db_name="persons",
        collection="pais",
        record_query={"_id": ObjectId('62f40218626954d099f69ee0')},
        record_new_value={
            "pais": "Canada",
            "ciudad": "Ottawa",
            
        }
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="pais"
    )
)