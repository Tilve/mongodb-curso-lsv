from bson.objectid import ObjectId
import sys

sys.path.append("..")
from car.databases.client import MongoLsv
mongo_lsv = MongoLsv()

print(
    mongo_lsv.delete_record_in_collection(
        db_name="persons",
        collection="actor_famoso",
        record_id="62f40218626954d099f69edf",
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="actor_famoso"
    )
)

print(
    mongo_lsv.delete_record_in_collection(
        db_name="persons",
        collection="páis",
        record_id="62f40218626954d099f69ee1",
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="pais"
    )
)
